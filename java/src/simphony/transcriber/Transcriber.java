package simphony.transcriber;

import java.util.ArrayList;
import simphony.datastruct.Pair;
import simphony.datastruct.PhonemeString;

public interface Transcriber {
	public void train(ArrayList<Pair<String, PhonemeString>> trainingData);
	public PhonemeString transcribe(String orth);
}

