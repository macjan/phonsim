package simphony.transcriber; 

import java.util.ArrayList;
import java.util.HashMap;
import com.ibm.icu.text.Transliterator;
import simphony.datastruct.Pair;
import simphony.datastruct.PhonemeString;

// TODO mappings -> file?, compile to own transliterator? develop into rule-based transcriber?

public class NaiveTranscriber implements Transcriber {
	public NaiveTranscriber() {}

	public void train(ArrayList<Pair<String, PhonemeString>> trainingData) {}

	public PhonemeString transcribe(String orth) {
		orth = transliterateToLatin(orth);
		orth = removeAccents(orth);
		orth = removeOtherDiacritics(orth);
		PhonemeString phon = toIpa(orth);
		// - assimilate voiceness
		// - detect glides (i/u next to vowels)
		// - remove double sounds
		// - remove sequences of two vowels (leave the first)
		// - remove 'h' between consonants
		return phon;
	}

	public static String removeAccents(String orth) {
		orth = Transliterator.getInstance("Accents-Any").transform(orth);
		return orth;
	}

	public static String removeOtherDiacritics(String orth) {
		StringBuilder resultSb = new StringBuilder();
		for (int i = 0; i < orth.length(); i++) {
			if (MAPPINGS.containsKey(orth.charAt(i)))
				resultSb.append(MAPPINGS.get(orth.charAt(i)));
			else if (Character.isLetter(orth.charAt(i)))
				resultSb.append(orth.charAt(i));
		}
		return resultSb.toString();
	}

	public static String transliterateToLatin(String orth) {
		orth = Transliterator.getInstance("Any-Latin").transform(orth);
		return orth;
	}

	private PhonemeString toIpa(String orth) {
		String ipaString = orth.replace("y", "i").replace("ng", "ŋ").replace("ch", "x").replace("c", "t͡s");
		return new PhonemeString(ipaString);
	}

	private static HashMap<Character, Character> MAPPINGS = new HashMap<Character, Character>();
	static {
		// ogonek
		MAPPINGS.put(new Character('ą'), new Character('a'));
		MAPPINGS.put(new Character('ę'), new Character('e'));
		MAPPINGS.put(new Character('į'), new Character('i'));
		MAPPINGS.put(new Character('ǫ'), new Character('o'));
		MAPPINGS.put(new Character('ų'), new Character('u'));

		// hacek
		MAPPINGS.put(new Character('ǎ'), new Character('a'));
		MAPPINGS.put(new Character('č'), new Character('c'));
		MAPPINGS.put(new Character('ě'), new Character('e'));
		MAPPINGS.put(new Character('ǐ'), new Character('i'));
		MAPPINGS.put(new Character('ǒ'), new Character('o'));
		MAPPINGS.put(new Character('š'), new Character('ʃ'));
		MAPPINGS.put(new Character('ǔ'), new Character('u'));
		MAPPINGS.put(new Character('ž'), new Character('ʒ'));

		// dot
		MAPPINGS.put(new Character('ż'), new Character('z'));
		
		// stroke
		MAPPINGS.put(new Character('đ'), new Character('d'));
		MAPPINGS.put(new Character('ł'), new Character('l'));

		// others
		MAPPINGS.put(new Character('ß'), new Character('s'));
	}
}

