package simphony.transcriber; 

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import simphony.datastruct.NGramWithContext;
import simphony.datastruct.Pair;
import simphony.datastruct.PhonemeString;
import simphony.algorithms.InstanceBasedLearner;
import simphony.algorithms.StochasticTransducer;

public class HmmTranscriber implements Transcriber, java.io.Serializable {
	private static final int NGRAM_LENGTH = 3;
	private static final int LEFT_CONTEXT_LENGTH = 2;
	private static final int RIGHT_CONTEXT_LENGTH = 2;

	private HashSet<PhonemeString> states;
	private HashMap<PhonemeString, Double> stateProbabilities;
	private HashMap<Pair<PhonemeString, PhonemeString>, Double> transitionProbabilities;

	public StochasticTransducer aligner;
	InstanceBasedLearner<Integer> letterChunker;
	InstanceBasedLearner<PhonemeString> phonemePredictor;

	public HmmTranscriber() {
		this.aligner = new StochasticTransducer();
		this.letterChunker = new InstanceBasedLearner<Integer>();
		this.phonemePredictor = new InstanceBasedLearner<PhonemeString>();
		this.states = new HashSet<PhonemeString>();
		this.stateProbabilities = new HashMap<PhonemeString, Double>();
		this.transitionProbabilities = new HashMap<Pair<PhonemeString, PhonemeString>, Double>();
	}

	public void train(ArrayList<Pair<String, PhonemeString>> trainingData) {
		this.aligner.train(trainingData);

		HashMap<PhonemeString, Integer> stateCounts = new HashMap<PhonemeString, Integer>();
		HashMap<Pair<PhonemeString, PhonemeString>, Integer> transitionCounts = 
			new HashMap<Pair<PhonemeString, PhonemeString>, Integer>();
		HashMap<PhonemeString, Integer> totalTransitionCounts =
			new HashMap<PhonemeString, Integer>();
		int totalStatesCount = 0;

		// process training data -- pair after pair
		for (Pair<String, PhonemeString> trainingPair : trainingData) {
			System.out.println(trainingPair.first + " " + trainingPair.second.toString());
			ArrayList<Pair<String, PhonemeString>> alignment = 
				this.aligner.align(trainingPair.first, trainingPair.second);

//			for (Pair<String, PhonemeString> alignmentPair : alignment)
//				System.out.println(" " + alignmentPair.first + " " + alignmentPair.second.toString());

			// train letter chunker
			this.letterChunker.addInstances(alignmentToChunkerInstances(trainingPair.first, alignment));

			// train phoneme predictor
			this.phonemePredictor.addInstances(alignmentToPredictorInstances(trainingPair.first, alignment));

			// add all occurring phonemes / phoneme clusters to HMM states and count them
			for (int i = 0; i < alignment.size(); i++) {
				PhonemeString state = alignment.get(i).second;
				this.states.add(state);
				totalStatesCount++;
				if (stateCounts.containsKey(state))
					stateCounts.put(state, stateCounts.get(state) + 1);
				else
					stateCounts.put(state, new Integer(1));
			}

			// count HMM transitions 
			for (int i = 0; i < alignment.size()-1; i++) {
				PhonemeString q1 = alignment.get(i).second;
				PhonemeString q2 = alignment.get(i+1).second;
				Pair<PhonemeString, PhonemeString> key = new Pair<PhonemeString, PhonemeString>(q1, q2);
				if (transitionCounts.containsKey(key))
					transitionCounts.put(key, transitionCounts.get(key) + 1);
				else
					transitionCounts.put(key, new Integer(1));
				if (totalTransitionCounts.containsKey(q1))
					totalTransitionCounts.put(q1, totalTransitionCounts.get(q1) + 1);
				else
					totalTransitionCounts.put(q1, new Integer(1));
			}
		}

		// normalize HMM state and transition counts into probabilities
		for (PhonemeString state : this.states) {
			this.stateProbabilities.put(state, new Double((double)(stateCounts.get(state)) / totalStatesCount));

			int totalTransitionsCount;
			if (totalTransitionCounts.containsKey(state)) {
				totalTransitionsCount = totalTransitionCounts.get(state);
				for (PhonemeString anotherState : this.states) {
					Pair<PhonemeString, PhonemeString> key = 
						new Pair<PhonemeString, PhonemeString>(state, anotherState);
					if (transitionCounts.containsKey(key)) {
						this.transitionProbabilities.put(key, new Double((double)(transitionCounts.get(key)) / totalTransitionsCount));
					}
					//else if (anotherState.length() == 1) {
					else {
						// TODO not a very elegant solution, maybe turn states to phonemes?
						this.transitionProbabilities.put(key, new Double(0.01d / totalTransitionsCount));
					}
					System.out.println(key.first + " -> " + key.second.toString() + " " + this.transitionProbabilities.get(key));
				}
			}
		}
		System.out.println("");
	}

	public PhonemeString transcribe(String orth) {
		ArrayList<NGramWithContext> instances = chunkingToInstances(chunkWord(orth));
		HashMap<Pair<NGramWithContext, PhonemeString>, Double> emissionProbabilities =
			computeEmissionProbabilities(instances);

		// Viterbi search algorithm -- find optimal path of phonemes
		HashMap<PhonemeString, PhonemeString> maximizingSequences = null;
		HashMap<PhonemeString, Double> maximizingProbabilities = null;
		for (NGramWithContext instance : instances) {

			StringBuilder lcSb = new StringBuilder();
			for (int i = 0; i < instance.getLeftContextSize(); i++)
				lcSb.append(instance.getLeftContextAt(i));
			StringBuilder rcSb = new StringBuilder();
			for (int i = 0; i < instance.getRightContextSize(); i++)
				rcSb.append(instance.getRightContextAt(i));
			System.out.println(lcSb.toString() + " " + instance.getNGram() + " " + rcSb.toString());

			HashMap<PhonemeString, PhonemeString> newMaximizingSequences = 
				new HashMap<PhonemeString, PhonemeString>();
			HashMap<PhonemeString, Double> newMaximizingProbabilities = 
				new HashMap<PhonemeString, Double>();
			// for each state: compute previous state probability and sequence
			for (PhonemeString state : this.states) {
				PhonemeString previousStateSequence;
				double previousStateProbability;
				// first iteration
				if (maximizingProbabilities == null) {
					previousStateSequence = new PhonemeString("");
					previousStateProbability = 1.0;
				}
				// not first iteration
				else {
					previousStateProbability = 0.0;
					PhonemeString previousState = null;
					for (PhonemeString s : this.states) {
						double sPr = 0.0;
						if (this.transitionProbabilities.containsKey(new Pair(s, state)))
							sPr = maximizingProbabilities.get(s) *
								this.transitionProbabilities.get(new Pair(s, state));
						if (sPr >= previousStateProbability) {
							previousStateProbability = sPr;
							previousState = s;
						}
						//if (sPr > 0.0)
							//System.out.println("" + s + " " + sPr);
					}
					previousStateSequence = maximizingSequences.get(previousState);
					//System.out.println("> " + state + " << " + previousStateSequence + " " + previousStateProbability);
				}
				newMaximizingSequences.put(state, previousStateSequence.concat(state));
				Pair<NGramWithContext, PhonemeString> key = 
					new Pair<NGramWithContext, PhonemeString>(instance, state);
				if (emissionProbabilities.containsKey(key))
					newMaximizingProbabilities.put(state, previousStateProbability * emissionProbabilities.get(key));
				else
					newMaximizingProbabilities.put(state, new Double(0.0));
			}
			maximizingSequences = newMaximizingSequences;
			maximizingProbabilities = newMaximizingProbabilities;
			System.out.println("");
		}

		// find the sequence with maximal probability
		PhonemeString maximizingSequence = null;
		double maxPr = 0.0;
		for (PhonemeString state : this.states) {
			double pr = maximizingProbabilities.get(state);
			if (pr >= maxPr) {
				maxPr = pr;
				maximizingSequence = maximizingSequences.get(state);
			}
		}
		if (maxPr == 0.0)
			System.out.println("ALARM!!!");
		//System.out.println("\n\n\n");
		System.out.println(maximizingSequence);
		return maximizingSequence;
	}

	private ArrayList<Pair<NGramWithContext, Integer>> alignmentToChunkerInstances(
		String orth, ArrayList<Pair<String, PhonemeString>> alignment) {
		ArrayList<Pair<NGramWithContext, Integer>> instances =
			new ArrayList<Pair<NGramWithContext, Integer>>();
		StringBuilder letterCombs = new StringBuilder();
		for (int i = 0; i < alignment.size(); i++)
			letterCombs.append(alignment.get(i).first + " ");
		System.out.println(letterCombs.toString());
		int j = 0, k = 0;
		for (int i = 0; i < orth.length(); i++) {
			String ngram = orth.substring(i, Math.min(i+NGRAM_LENGTH, orth.length()));
			NGramWithContext nGramWithContext = new NGramWithContext(ngram);
			for (int c = 1; c <= Math.min(i, LEFT_CONTEXT_LENGTH); c++)
				nGramWithContext.addLeftContext(orth.substring(i-c, i-c+1));
			for (int c = 1; c <= Math.min(orth.length()-i-NGRAM_LENGTH, RIGHT_CONTEXT_LENGTH); c++)
				nGramWithContext.addRightContext(orth.substring(i+NGRAM_LENGTH+c-1, i+NGRAM_LENGTH+c));
			int label = 0;
			for (int n = 1; n <= Math.min(NGRAM_LENGTH, orth.length()-i); n++)
				if (ngram.substring(0, n).equals(alignment.get(j).first))
					label = n;

			StringBuilder leftContext = new StringBuilder();
			for (int q = 0; q < nGramWithContext.getLeftContextSize(); q++)
				leftContext.append(nGramWithContext.getLeftContextAt(q));
			StringBuilder rightContext = new StringBuilder();
			for (int q = 0; q < nGramWithContext.getRightContextSize(); q++)
				rightContext.append(nGramWithContext.getRightContextAt(q));
			System.out.println(leftContext.toString() + " " + ngram + " " + rightContext.toString() + " " + label + " " + alignment.get(j).first);

			instances.add(new Pair<NGramWithContext, Integer>(
				nGramWithContext, new Integer(label)));

			k += 1;
			if (k == alignment.get(j).first.length()) {
				k = 0;
				j += 1;
			}
		}
		System.out.println("");
		return instances;
	}

	private ArrayList<Pair<NGramWithContext, PhonemeString>> alignmentToPredictorInstances(
		String orth, ArrayList<Pair<String, PhonemeString>> alignment) {
		ArrayList<Pair<NGramWithContext, PhonemeString>> instances =
			new ArrayList<Pair<NGramWithContext, PhonemeString>>();

		int i = 0;
		for (int j = 0; j < alignment.size(); j++) {
			String ngram = alignment.get(j).first;
			NGramWithContext nGramWithContext = new NGramWithContext(ngram);
			for (int c = 1; c <= Math.min(i, LEFT_CONTEXT_LENGTH); c++)
				nGramWithContext.addLeftContext(orth.substring(i-c, i-c+1));
			for (int c = 1; c <= Math.min(orth.length()-ngram.length()-i-1, RIGHT_CONTEXT_LENGTH); c++)
				nGramWithContext.addRightContext(orth.substring(i+ngram.length()+c-1, i+ngram.length()+c));
			instances.add(new Pair<NGramWithContext, PhonemeString>(
				nGramWithContext, alignment.get(j).second));
			i += ngram.length();

			StringBuilder leftContext = new StringBuilder();
			for (int q = 0; q < nGramWithContext.getLeftContextSize(); q++)
				leftContext.append(nGramWithContext.getLeftContextAt(q));
			StringBuilder rightContext = new StringBuilder();
			for (int q = 0; q < nGramWithContext.getRightContextSize(); q++)
				rightContext.append(nGramWithContext.getRightContextAt(q));
			System.out.println(leftContext.toString() + " " + ngram + " " + rightContext.toString() + " " + alignment.get(j).second.toString());
		}
		return instances;
	}

	private ArrayList<NGramWithContext> wordToInstances(String word) {
		ArrayList<NGramWithContext> instances = new ArrayList<NGramWithContext>();
		for (int i = 0; i < word.length(); i++) {
			String ngram = word.substring(i, Math.min(i+NGRAM_LENGTH, word.length()));
			NGramWithContext nGramWithContext = new NGramWithContext(ngram);
			for (int c = 1; c <= Math.min(i, LEFT_CONTEXT_LENGTH); c++)
				nGramWithContext.addLeftContext(word.substring(i-c, i-c+1));
			for (int c = 1; c <= Math.min(word.length()-NGRAM_LENGTH-i-1, RIGHT_CONTEXT_LENGTH); c++)
				nGramWithContext.addRightContext(word.substring(i+NGRAM_LENGTH+c, i+NGRAM_LENGTH+c+1));
			instances.add(nGramWithContext);
		}
		return instances;
	}

	private ArrayList<String> chunkWord(String word) {
		ArrayList<String> chunks = new ArrayList<String>();
		int ignore = 0;
		for (NGramWithContext instance : wordToInstances(word)) {
			if (ignore > 0) {
				ignore -= 1;
				continue;
			}
			int label = this.letterChunker.classify(instance);
			if (label == 0) label = 1;
			if (label > instance.getNGram().length()) label = instance.getNGram().length();
			chunks.add(instance.getNGram().substring(0, label));
			ignore = label-1;
		}
		for (int i = 0; i < chunks.size(); i++) System.out.println("|"+chunks.get(i));
		return chunks;
	}

	private ArrayList<NGramWithContext> chunkingToInstances(ArrayList<String> chunking) {
		ArrayList<NGramWithContext> instances = new ArrayList<NGramWithContext>();
		System.out.println(chunking.size());

		// reconstruct word from chunking
		StringBuilder orthSb = new StringBuilder();
		for (String chunk : chunking)
			orthSb.append(chunk);
		String orth = orthSb.toString();

		// create instances
		int i = 0;
		for (int j = 0; j < chunking.size(); j++) {
			String ngram = chunking.get(j);
			NGramWithContext nGramWithContext = new NGramWithContext(ngram);
			for (int c = 1; c <= Math.min(i, LEFT_CONTEXT_LENGTH); c++)
				nGramWithContext.addLeftContext(orth.substring(i-c, i-c+1));
			for (int c = 1; c <= Math.min(orth.length()-ngram.length()-i, RIGHT_CONTEXT_LENGTH); c++)
				nGramWithContext.addRightContext(orth.substring(i+ngram.length()+c-1, i+ngram.length()+c));
			instances.add(nGramWithContext);
			i += ngram.length();
		}

		System.out.println(instances.size());
		return instances;
	}

	private HashMap<Pair<NGramWithContext, PhonemeString>, Double> computeEmissionProbabilities(
		ArrayList<NGramWithContext> instances) {

		HashMap<Pair<NGramWithContext, PhonemeString>, Double> emissionProbabilities = 
			new HashMap<Pair<NGramWithContext, PhonemeString>, Double>();

		for (NGramWithContext instance : instances) {
			System.out.println(instance.getNGram());
			HashMap<PhonemeString, Double> predictions = this.phonemePredictor.allLabels(instance);

			// get rid of too improbable predictions
			double smallestDist = 1.0;
			for (Double dist : predictions.values())
				if (dist < smallestDist)
					smallestDist = dist;
			if (smallestDist == 1.0)
				System.out.println("ERROR: no predictions!!!");
			for (PhonemeString prediction : ((HashMap<PhonemeString, Double>)(predictions.clone())).keySet()) {
				if (predictions.get(prediction) > smallestDist + 0.2) {
					predictions.remove(prediction);
				}
			}

			double totalConfidence = 0.0;
			for (Double distance : predictions.values())
				totalConfidence += 1.0d - distance;

			// compute probabilities from Bayes' formula
			for (PhonemeString prediction : predictions.keySet()) {
				double confidence = 1.0d - predictions.get(prediction);
				Pair<NGramWithContext, PhonemeString> key = 
					new Pair<NGramWithContext, PhonemeString>(instance, prediction);
				//emissionProbabilities.put(key, new Double(confidence / totalConfidence /
				//	this.stateProbabilities.get(prediction)));
				emissionProbabilities.put(key, new Double(confidence / totalConfidence));
				System.out.println("" + instance.getNGram() + " " + prediction.toString() + " " + confidence / totalConfidence);
			}
		}

		// compute emission probabilities from Bayes formula
		return emissionProbabilities;
	}
}
