package simphony.datastruct;

public class Pair<X, Y> implements java.io.Serializable {
	public final X first;
	public final Y second;

	public Pair(X first, Y second) {
		this.first = first;
		this.second = second;
	}

	@Override
	public boolean equals(Object otherPair) {
		if (!(otherPair instanceof Pair)) return false;
		return (this.first.equals(((Pair<X, Y>)otherPair).first) && 
			this.second.equals(((Pair<X, Y>)otherPair).second));
	}

	@Override
	public int hashCode() {
		return this.first.hashCode() + this.second.hashCode();
	}
}
