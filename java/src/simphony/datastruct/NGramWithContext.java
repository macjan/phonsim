package simphony.datastruct;

import java.util.ArrayList;

/**
 * @author Maciej Janicki
*/

public class NGramWithContext implements java.io.Serializable {
	String ngram;
	int leftContextSize, rightContextSize;
	ArrayList<String> leftContext, rightContext;

	public NGramWithContext(String ngram) {
		this.ngram = ngram;
		this.leftContextSize = 0;
		this.rightContextSize = 0;
		this.leftContext = new ArrayList<String>();
		this.rightContext = new ArrayList<String>();
	}

	@Override
	public boolean equals(Object other) {
		if (!(other instanceof NGramWithContext))
			return false;
		NGramWithContext otherNGram = (NGramWithContext)other;
		if (!this.ngram.equals(otherNGram.getNGram()))
			return false;
		if (this.leftContextSize != otherNGram.getLeftContextSize())
			return false;
		if (this.rightContextSize != otherNGram.getRightContextSize())
			return false;
		for (int i = 0; i < this.leftContextSize; i++) {
			if (!this.getLeftContextAt(i).equals(otherNGram.getLeftContextAt(i)))
				return false;
		}
		for (int i = 0; i < this.rightContextSize; i++) {
			if (!this.getRightContextAt(i).equals(otherNGram.getRightContextAt(i)))
				return false;
		}
		return true;
	}

	public int getLeftContextSize() {
		return this.leftContextSize;
	}

	public int getRightContextSize() {
		return this.rightContextSize;
	}

	public void addLeftContext(String context) {
		this.leftContext.add(0, context);
		this.leftContextSize += 1;
	}

	public void addRightContext(String context) {
		this.rightContext.add(context);
		this.rightContextSize += 1;
	}

	public String getLeftContextAt(int pos) {
		return this.leftContext.get(pos);
	}

	public String getRightContextAt(int pos) {
		return this.rightContext.get(pos);
	}

	public String getNGram() {
		return new String(this.ngram);
	}

	public double distance(NGramWithContext other) {
		// TODO weights - farther contexts have smaller importance
		if (!this.ngram.equals(other.ngram))
			return 1.0;
		double dist = 0.0, maxDist = 1.0;
		// compare left context
		for (int i = 0; i < Math.max(this.leftContextSize, other.getLeftContextSize()); i++) {
//			if (i >= this.leftContextSize) dist += 1.0;
//			else if (i >= other.getLeftContextSize()) dist += 1.0;
//			else if (!this.getLeftContextAt(this.leftContextSize-i-1).equals(
//				other.getLeftContextAt(other.getLeftContextSize()-i-1))) dist += 1.0;
			// TODO if no context at both instances -- no difference
			if ((i >= this.leftContextSize) && (i >= other.getLeftContextSize()));
			else if (i >= this.leftContextSize) dist += 1.0 / (i+1);
			else if (i >= other.getLeftContextSize()) dist += 1.0 / (i+1);
			else if (!this.getLeftContextAt(this.leftContextSize-i-1).equals(
				other.getLeftContextAt(other.getLeftContextSize()-i-1))) dist += 1.0 / (i+1);
			maxDist += 1.0 / (i+1);
			//maxDist += 1.0;
		}
		// compare right context
		for (int i = 0; i < Math.max(this.rightContextSize, other.getRightContextSize()); i++) {
//			if (i >= this.rightContextSize) dist += 1.0;
//			else if (i >= other.getRightContextSize()) dist += 1.0;
//			else if (!this.getRightContextAt(i).equals(other.getRightContextAt(i))) dist += 1.0;
			if ((i >= this.rightContextSize) && (i >= other.getRightContextSize()));
			else if (i >= this.rightContextSize) dist += 1.0 / (i+1);
			else if (i >= other.getRightContextSize()) dist += 1.0 / (i+1);
			else if (!this.getRightContextAt(i).equals(other.getRightContextAt(i))) dist += 1.0 / (i+1);
			maxDist += 1.0 / (i+1);
			//maxDist += 1.0;
		}
		//double maxDist = Math.max(this.getLeftContextSize(), other.getLeftContextSize()) +
		//	Math.max(this.getRightContextSize(), other.getRightContextSize());
		//return Math.sqrt(dist / maxDist);
		return dist / maxDist;
	}
}
