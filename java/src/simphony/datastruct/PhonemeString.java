package simphony.datastruct;

import java.util.ArrayList;
import java.util.Set;

public class PhonemeString implements java.io.Serializable {
	private static final Set<String> IPA_PHONEME_SYMBOLS = PhonemeFeatures.PHONEMES.keySet();
	private static final Set<String> IPA_MODIFIER_SYMBOLS = PhonemeFeatures.MODIFIERS.keySet();

	public static final String IPA_ARC = "\u0361";
	public static final String IPA_DENTAL = "\u032a";

	private ArrayList<Phoneme> phonemes;

	public PhonemeString(String orth) {
		this.phonemes = new ArrayList<Phoneme>();
		orth = removeTildes(orth);

		// compose a phoneme sequence from string
		StringBuilder phonemeStringBuilder = new StringBuilder();
		StringBuilder modifiersStringBuilder = new StringBuilder();
		boolean expectingPhonemeSymbol = true;
		for (int i = 0; i < orth.length(); i++) {
			String c = orth.substring(i, i+1);
			if (IPA_PHONEME_SYMBOLS.contains(c)) {
				if (!expectingPhonemeSymbol) {
					if (IPA_PHONEME_SYMBOLS.contains(phonemeStringBuilder.toString()))
						this.phonemes.add(new Phoneme(
							phonemeStringBuilder.toString(), modifiersStringBuilder.toString()));
					phonemeStringBuilder = new StringBuilder();
					modifiersStringBuilder = new StringBuilder();
				}
				phonemeStringBuilder.append(c);
				expectingPhonemeSymbol = false;
			} else if (IPA_MODIFIER_SYMBOLS.contains(c)) {
				//phonemeStringBuilder.append(c);
				modifiersStringBuilder.append(c);
				expectingPhonemeSymbol = false;
			} else if (c.equals(IPA_ARC)) {
				phonemeStringBuilder.append(c);
				expectingPhonemeSymbol = true;
			} else if (c.equals(IPA_DENTAL)) {
				// add dental symbol if it makes a known phoneme symbol; otherwise ignore
				if (IPA_PHONEME_SYMBOLS.contains((phonemeStringBuilder.toString() + c)))
					phonemeStringBuilder.append(c);
				expectingPhonemeSymbol = false;
			} else {
				System.out.println("Unknown symbol: " + c);
			}
		}
		if (phonemeStringBuilder.length() > 0)
			this.phonemes.add(new Phoneme(
				phonemeStringBuilder.toString(), modifiersStringBuilder.toString()));
	}

	public PhonemeString(ArrayList<Phoneme> phonemes) {
		this.phonemes = phonemes;
	}

	@Override
	public boolean equals(Object other) {
		if (!(other instanceof PhonemeString)) return false;
		//ArrayList<Phoneme> otherPhonemes = other.getPhonemes();
		//System.out.println(this.toString() + " " + other.toString() + " " + this.toString().equals(other.toString()));
		return this.phonemes.equals(((PhonemeString)other).getPhonemes());
	}

	@Override
	public int hashCode() {
		int code = 0;
		for (Phoneme phoneme : this.phonemes) {
			code += phoneme.hashCode();
		}
		return code;
		//return this.toString().hashCode();
	}

	public int length() {
		return this.phonemes.size();
	}

	public ArrayList<Phoneme> getPhonemes() {
		return this.phonemes;
	}

	public PhonemeString substring(int beginIndex, int endIndex) {
		ArrayList<Phoneme> subStrPhonemes = new ArrayList<Phoneme>();
		for (int i = beginIndex; i < endIndex; i++)
			subStrPhonemes.add(this.phonemes.get(i));
		return new PhonemeString(subStrPhonemes);
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (Phoneme phoneme : this.phonemes)
			sb.append(phoneme.toString());
		return sb.toString();
	}

	// concatenation
	public PhonemeString concat(PhonemeString other) {
		PhonemeString result = new PhonemeString("");
		for (Phoneme phoneme : this.phonemes)
			result.phonemes.add(phoneme);
		for (Phoneme phoneme : other.phonemes)
			result.phonemes.add(phoneme);
		return result;
	}

	// turn non-combining tildes into combining
	private String removeTildes(String orth) {
		orth = orth.replace("ã", "a\u0303").replace("ẽ", "e\u0303").replace("ĩ", "i\u0303")
			.replace("õ", "o\u0303").replace("ũ", "u\u0303");
		return orth;
	}
}
