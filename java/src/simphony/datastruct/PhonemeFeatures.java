package simphony.datastruct;

import java.util.HashMap;

public class PhonemeFeatures implements java.io.Serializable {
	// place of articulation
	public static final double PFP_BILABIAL		= 1.0d;
	public static final double PFP_LABIODENTAL	= 0.95d;
	public static final double PFP_DENTAL		= 0.725d;
	public static final double PFP_ALVEOLAR		= 0.7d;
	public static final double PFP_POSTALVEOLAR	= 0.65d;
	public static final double PFP_RETROFLEX	= 0.6d;
	public static final double PFP_PALATAL		= 0.4d;
	public static final double PFP_VELAR		= 0.3d;
	public static final double PFP_UVULAR		= 0.2d;
	public static final double PFP_PHARYNGEAL	= 0.15d;
	public static final double PFP_EPIGLOTTAL	= 0.125d;
	public static final double PFP_GLOTTAL		= 0.1d;
	public static final double PFP_NOART		= 0.0d;

	// manner of articulation
	public static final double PFM_PLOSIVE		= 1.0d;
	public static final double PFM_AFFRICATE	= 0.9d;
	public static final double PFM_FRICATIVE	= 0.8d;
	public static final double PFM_APPROXIMANT	= 0.5d;
	public static final double PFM_VOWEL_C		= 0.3d;
	public static final double PFM_VOWEL_NC		= 0.275d;
	public static final double PFM_VOWEL_CM		= 0.25d;
	public static final double PFM_VOWEL_M		= 0.2d;
	public static final double PFM_VOWEL_OM		= 0.15d;
	public static final double PFM_VOWEL_NO		= 0.125d;
	public static final double PFM_VOWEL_O		= 0.1d;
	public static final double PFM_NOMANNER		= 0.0d;

	// flags
	public static final int PFF_NOFLAGS		= 0x0000;
	public static final int PFF_VOICED		= 0x0001;
	public static final int PFF_ASPIRATED	= 0x0002;
	public static final int PFF_EJECTIVE	= 0x0004;
	public static final int PFF_IMPLOSIVE	= 0x0008;
	public static final int PFF_NASAL		= 0x0010;
	public static final int PFF_LATERAL		= 0x0020;
	public static final int PFF_LONG		= 0x0040;
	public static final int PFF_NONSYLLABIC	= 0x0080;
	public static final int PFF_FULL		= 0xFFFF;

	public double place;
	public double manner;
	public double secondary;
	public int flags;

	public PhonemeFeatures(double place, double manner, double secondary, int flags) {
		this.place = place;
		this.manner = manner;
		this.secondary = secondary;
		this.flags = flags;
	}

	// copying constructor
	public PhonemeFeatures(PhonemeFeatures other) {
		this.place = other.place;
		this.manner = other.manner;
		this.secondary = other.secondary;
		this.flags = other.flags;
	}

	public static PhonemeFeatures getFeatures(String basicPhoneme, String modifiers) {
		PhonemeFeatures features = null;
		if (PHONEMES.containsKey(basicPhoneme)) {
			features = new PhonemeFeatures(PHONEMES.get(basicPhoneme));
		}
		else {
			System.out.println("Unknown symbol: " + basicPhoneme);
			return null;		// TODO exception?
		}

		// apply modifiers
		for (int i = 0; i < modifiers.length(); i++) {
			if (MODIFIERS.containsKey(modifiers.substring(i, i+1))) {
				PhonemeFeatures modFeatures = MODIFIERS.get(modifiers.substring(i, i+1));

				if (modFeatures.secondary != PFP_NOART)
					features.secondary = modFeatures.secondary;
				features.flags |= modFeatures.flags;
			}
		}

		return features;
	}

	// THE BIG TABLE OF PHONEME FEATURES
	public static final HashMap<String, PhonemeFeatures> PHONEMES = 
		new HashMap<String, PhonemeFeatures>();
	static {
		// nasal stops (only voiced)
		PHONEMES.put(new String("m"), new PhonemeFeatures(PFP_BILABIAL, PFM_PLOSIVE, PFP_NOART, PFF_VOICED | PFF_NASAL));
		PHONEMES.put(new String("ɱ"), new PhonemeFeatures(PFP_LABIODENTAL, PFM_PLOSIVE, PFP_NOART, PFF_VOICED | PFF_NASAL));
		PHONEMES.put(new String("n̪"), new PhonemeFeatures(PFP_DENTAL, PFM_PLOSIVE, PFP_NOART, PFF_VOICED | PFF_NASAL));
		PHONEMES.put(new String("n"), new PhonemeFeatures(PFP_ALVEOLAR, PFM_PLOSIVE, PFP_NOART, PFF_VOICED | PFF_NASAL));
		PHONEMES.put(new String("ɳ"), new PhonemeFeatures(PFP_RETROFLEX, PFM_PLOSIVE, PFP_NOART, PFF_VOICED | PFF_NASAL));
		PHONEMES.put(new String("ɲ"), new PhonemeFeatures(PFP_PALATAL, PFM_PLOSIVE, PFP_NOART, PFF_VOICED | PFF_NASAL));
		PHONEMES.put(new String("ŋ"), new PhonemeFeatures(PFP_VELAR, PFM_PLOSIVE, PFP_NOART, PFF_VOICED | PFF_NASAL));
		PHONEMES.put(new String("ɴ"), new PhonemeFeatures(PFP_UVULAR, PFM_PLOSIVE, PFP_NOART, PFF_VOICED | PFF_NASAL));
		
		// voiceless plosives
		PHONEMES.put(new String("p"), new PhonemeFeatures(PFP_BILABIAL, PFM_PLOSIVE, PFP_NOART, PFF_NOFLAGS));
		PHONEMES.put(new String("p̪"), new PhonemeFeatures(PFP_LABIODENTAL, PFM_PLOSIVE, PFP_NOART, PFF_NOFLAGS));
		PHONEMES.put(new String("t̪"), new PhonemeFeatures(PFP_DENTAL, PFM_PLOSIVE, PFP_NOART, PFF_NOFLAGS));
		PHONEMES.put(new String("t"), new PhonemeFeatures(PFP_ALVEOLAR, PFM_PLOSIVE, PFP_NOART, PFF_NOFLAGS));
		PHONEMES.put(new String("ʈ"), new PhonemeFeatures(PFP_RETROFLEX, PFM_PLOSIVE, PFP_NOART, PFF_NOFLAGS));
		PHONEMES.put(new String("c"), new PhonemeFeatures(PFP_PALATAL, PFM_PLOSIVE, PFP_NOART, PFF_NOFLAGS));
		PHONEMES.put(new String("k"), new PhonemeFeatures(PFP_VELAR, PFM_PLOSIVE, PFP_NOART, PFF_NOFLAGS));
		PHONEMES.put(new String("q"), new PhonemeFeatures(PFP_UVULAR, PFM_PLOSIVE, PFP_NOART, PFF_NOFLAGS));
		PHONEMES.put(new String("ʡ"), new PhonemeFeatures(PFP_EPIGLOTTAL, PFM_PLOSIVE, PFP_NOART, PFF_NOFLAGS));
		PHONEMES.put(new String("ʔ"), new PhonemeFeatures(PFP_GLOTTAL, PFM_PLOSIVE, PFP_NOART, PFF_NOFLAGS));

		// voiced plosives
		PHONEMES.put(new String("b"), new PhonemeFeatures(PFP_BILABIAL, PFM_PLOSIVE, PFP_NOART, PFF_VOICED));
		PHONEMES.put(new String("b̪"), new PhonemeFeatures(PFP_LABIODENTAL, PFM_PLOSIVE, PFP_NOART, PFF_VOICED));
		PHONEMES.put(new String("d̪"), new PhonemeFeatures(PFP_DENTAL, PFM_PLOSIVE, PFP_NOART, PFF_VOICED));
		PHONEMES.put(new String("d"), new PhonemeFeatures(PFP_ALVEOLAR, PFM_PLOSIVE, PFP_NOART, PFF_VOICED));
		PHONEMES.put(new String("ɖ"), new PhonemeFeatures(PFP_RETROFLEX, PFM_PLOSIVE, PFP_NOART, PFF_VOICED));
		PHONEMES.put(new String("ɟ"), new PhonemeFeatures(PFP_PALATAL, PFM_PLOSIVE, PFP_NOART, PFF_VOICED));
		PHONEMES.put(new String("g"), new PhonemeFeatures(PFP_VELAR, PFM_PLOSIVE, PFP_NOART, PFF_VOICED));
		PHONEMES.put(new String("ɡ"), new PhonemeFeatures(PFP_VELAR, PFM_PLOSIVE, PFP_NOART, PFF_VOICED));
		PHONEMES.put(new String("ɢ"), new PhonemeFeatures(PFP_UVULAR, PFM_PLOSIVE, PFP_NOART, PFF_VOICED));

		// implosive stops
		PHONEMES.put(new String("ɓ"), new PhonemeFeatures(PFP_BILABIAL, PFM_PLOSIVE, PFP_NOART, PFF_VOICED | PFF_IMPLOSIVE));
		PHONEMES.put(new String("ɗ"), new PhonemeFeatures(PFP_ALVEOLAR, PFM_PLOSIVE, PFP_NOART, PFF_VOICED | PFF_IMPLOSIVE));
		PHONEMES.put(new String("ᶑ"), new PhonemeFeatures(PFP_RETROFLEX, PFM_PLOSIVE, PFP_NOART, PFF_VOICED | PFF_IMPLOSIVE));
		PHONEMES.put(new String("ʄ"), new PhonemeFeatures(PFP_PALATAL, PFM_PLOSIVE, PFP_NOART, PFF_VOICED | PFF_IMPLOSIVE));
		PHONEMES.put(new String("ɠ"), new PhonemeFeatures(PFP_VELAR, PFM_PLOSIVE, PFP_NOART, PFF_VOICED | PFF_IMPLOSIVE));
		PHONEMES.put(new String("ʛ"), new PhonemeFeatures(PFP_UVULAR, PFM_PLOSIVE, PFP_NOART, PFF_VOICED | PFF_IMPLOSIVE));

		// voiceless affricates
		PHONEMES.put(new String("p͡f"), new PhonemeFeatures(PFP_BILABIAL, PFM_AFFRICATE, PFP_NOART, PFF_NOFLAGS));
		PHONEMES.put(new String("t͡s"), new PhonemeFeatures(PFP_ALVEOLAR, PFM_AFFRICATE, PFP_NOART, PFF_NOFLAGS));
		PHONEMES.put(new String("t͡ʃ"), new PhonemeFeatures(PFP_POSTALVEOLAR, PFM_AFFRICATE, PFP_NOART, PFF_NOFLAGS));
		PHONEMES.put(new String("ʈ͡ʂ"), new PhonemeFeatures(PFP_RETROFLEX, PFM_AFFRICATE, PFP_NOART, PFF_NOFLAGS));
		PHONEMES.put(new String("t͡ʂ"), new PhonemeFeatures(PFP_RETROFLEX, PFM_AFFRICATE, PFP_NOART, PFF_NOFLAGS));
		PHONEMES.put(new String("t͡ɕ"), new PhonemeFeatures(PFP_PALATAL, PFM_AFFRICATE, PFP_NOART, PFF_NOFLAGS));
		PHONEMES.put(new String("c͡ç"), new PhonemeFeatures(PFP_PALATAL, PFM_AFFRICATE, PFP_NOART, PFF_NOFLAGS));
		PHONEMES.put(new String("k͡x"), new PhonemeFeatures(PFP_VELAR, PFM_AFFRICATE, PFP_NOART, PFF_NOFLAGS));
		PHONEMES.put(new String("q͡χ"), new PhonemeFeatures(PFP_UVULAR, PFM_AFFRICATE, PFP_NOART, PFF_NOFLAGS));

		// voiced affricates
		PHONEMES.put(new String("d͡z"), new PhonemeFeatures(PFP_ALVEOLAR, PFM_AFFRICATE, PFP_NOART, PFF_VOICED));
		PHONEMES.put(new String("d͡ʒ"), new PhonemeFeatures(PFP_POSTALVEOLAR, PFM_AFFRICATE, PFP_NOART, PFF_VOICED));
		PHONEMES.put(new String("d͡ʑ"), new PhonemeFeatures(PFP_RETROFLEX, PFM_AFFRICATE, PFP_NOART, PFF_VOICED));
		PHONEMES.put(new String("ɖ͡ʐ"), new PhonemeFeatures(PFP_PALATAL, PFM_AFFRICATE, PFP_NOART, PFF_VOICED));
		PHONEMES.put(new String("d͡ʐ"), new PhonemeFeatures(PFP_PALATAL, PFM_AFFRICATE, PFP_NOART, PFF_VOICED));
		PHONEMES.put(new String("ɟ͡ʝ"), new PhonemeFeatures(PFP_PALATAL, PFM_AFFRICATE, PFP_NOART, PFF_VOICED));
		PHONEMES.put(new String("ɡ͡ɣ"), new PhonemeFeatures(PFP_VELAR, PFM_AFFRICATE, PFP_NOART, PFF_VOICED));
		PHONEMES.put(new String("ɢ͡ʁ"), new PhonemeFeatures(PFP_UVULAR, PFM_AFFRICATE, PFP_NOART, PFF_VOICED));

		// voiceless fricatives
		PHONEMES.put(new String("ɸ"), new PhonemeFeatures(PFP_BILABIAL, PFM_FRICATIVE, PFP_NOART, PFF_NOFLAGS));
		PHONEMES.put(new String("f"), new PhonemeFeatures(PFP_LABIODENTAL, PFM_FRICATIVE, PFP_NOART, PFF_NOFLAGS));
		PHONEMES.put(new String("θ"), new PhonemeFeatures(PFP_DENTAL, PFM_FRICATIVE, PFP_NOART, PFF_NOFLAGS));
		PHONEMES.put(new String("s"), new PhonemeFeatures(PFP_ALVEOLAR, PFM_FRICATIVE, PFP_NOART, PFF_NOFLAGS));
		PHONEMES.put(new String("ɬ"), new PhonemeFeatures(PFP_ALVEOLAR, PFM_FRICATIVE, PFP_NOART, PFF_LATERAL));
		PHONEMES.put(new String("ʃ"), new PhonemeFeatures(PFP_POSTALVEOLAR, PFM_FRICATIVE, PFP_NOART, PFF_NOFLAGS));
		PHONEMES.put(new String("ʂ"), new PhonemeFeatures(PFP_RETROFLEX, PFM_FRICATIVE, PFP_NOART, PFF_NOFLAGS));
		PHONEMES.put(new String("ç"), new PhonemeFeatures(PFP_PALATAL, PFM_FRICATIVE, PFP_NOART, PFF_NOFLAGS));
		PHONEMES.put(new String("ɕ"), new PhonemeFeatures(PFP_PALATAL, PFM_FRICATIVE, PFP_NOART, PFF_NOFLAGS));
		PHONEMES.put(new String("x"), new PhonemeFeatures(PFP_VELAR, PFM_FRICATIVE, PFP_NOART, PFF_NOFLAGS));
		PHONEMES.put(new String("χ"), new PhonemeFeatures(PFP_UVULAR, PFM_FRICATIVE, PFP_NOART, PFF_NOFLAGS));
		PHONEMES.put(new String("ħ"), new PhonemeFeatures(PFP_PHARYNGEAL, PFM_FRICATIVE, PFP_NOART, PFF_NOFLAGS));
		PHONEMES.put(new String("ʜ"), new PhonemeFeatures(PFP_EPIGLOTTAL, PFM_FRICATIVE, PFP_NOART, PFF_NOFLAGS));
		PHONEMES.put(new String("h"), new PhonemeFeatures(PFP_GLOTTAL, PFM_FRICATIVE, PFP_NOART, PFF_NOFLAGS));

		// voiced fricatives
		PHONEMES.put(new String("β"), new PhonemeFeatures(PFP_BILABIAL, PFM_FRICATIVE, PFP_NOART, PFF_VOICED));
		PHONEMES.put(new String("v"), new PhonemeFeatures(PFP_LABIODENTAL, PFM_FRICATIVE, PFP_NOART, PFF_VOICED));
		PHONEMES.put(new String("ð"), new PhonemeFeatures(PFP_DENTAL, PFM_FRICATIVE, PFP_NOART, PFF_VOICED));
		PHONEMES.put(new String("z"), new PhonemeFeatures(PFP_ALVEOLAR, PFM_FRICATIVE, PFP_NOART, PFF_VOICED));
		PHONEMES.put(new String("ɮ"), new PhonemeFeatures(PFP_ALVEOLAR, PFM_FRICATIVE, PFP_NOART, PFF_LATERAL | PFF_VOICED));
		PHONEMES.put(new String("ʒ"), new PhonemeFeatures(PFP_POSTALVEOLAR, PFM_FRICATIVE, PFP_NOART, PFF_VOICED));
		PHONEMES.put(new String("ʐ"), new PhonemeFeatures(PFP_RETROFLEX, PFM_FRICATIVE, PFP_NOART, PFF_VOICED));
		PHONEMES.put(new String("ʝ"), new PhonemeFeatures(PFP_PALATAL, PFM_FRICATIVE, PFP_NOART, PFF_VOICED));
		PHONEMES.put(new String("ʑ"), new PhonemeFeatures(PFP_PALATAL, PFM_FRICATIVE, PFP_NOART, PFF_VOICED));
		PHONEMES.put(new String("ɣ"), new PhonemeFeatures(PFP_VELAR, PFM_FRICATIVE, PFP_NOART, PFF_VOICED));
		PHONEMES.put(new String("ʁ"), new PhonemeFeatures(PFP_UVULAR, PFM_FRICATIVE, PFP_NOART, PFF_VOICED));
		PHONEMES.put(new String("ʕ"), new PhonemeFeatures(PFP_PHARYNGEAL, PFM_FRICATIVE, PFP_NOART, PFF_VOICED));
		PHONEMES.put(new String("ʢ"), new PhonemeFeatures(PFP_EPIGLOTTAL, PFM_FRICATIVE, PFP_NOART, PFF_VOICED));
		PHONEMES.put(new String("ɦ"), new PhonemeFeatures(PFP_GLOTTAL, PFM_FRICATIVE, PFP_NOART, PFF_VOICED));

		// approximants (only voiced), flaps, taps, trills, laterals
		PHONEMES.put(new String("ʙ"), new PhonemeFeatures(PFP_BILABIAL, PFM_APPROXIMANT, PFP_NOART, PFF_VOICED));
		PHONEMES.put(new String("ʋ"), new PhonemeFeatures(PFP_LABIODENTAL, PFM_APPROXIMANT, PFP_NOART, PFF_VOICED));
		PHONEMES.put(new String("l̪"), new PhonemeFeatures(PFP_DENTAL, PFM_APPROXIMANT, PFP_NOART, PFF_LATERAL | PFF_VOICED));
		PHONEMES.put(new String("ɹ"), new PhonemeFeatures(PFP_ALVEOLAR, PFM_APPROXIMANT, PFP_NOART, PFF_VOICED));
		PHONEMES.put(new String("ɾ"), new PhonemeFeatures(PFP_ALVEOLAR, PFM_APPROXIMANT, PFP_NOART, PFF_VOICED));
		PHONEMES.put(new String("r"), new PhonemeFeatures(PFP_ALVEOLAR, PFM_APPROXIMANT, PFP_NOART, PFF_VOICED));
		PHONEMES.put(new String("l"), new PhonemeFeatures(PFP_ALVEOLAR, PFM_APPROXIMANT, PFP_NOART, PFF_LATERAL | PFF_VOICED));
		PHONEMES.put(new String("ɽ"), new PhonemeFeatures(PFP_RETROFLEX, PFM_APPROXIMANT, PFP_NOART, PFF_VOICED));
		PHONEMES.put(new String("ɭ"), new PhonemeFeatures(PFP_RETROFLEX, PFM_APPROXIMANT, PFP_NOART, PFF_LATERAL | PFF_VOICED));
		PHONEMES.put(new String("j"), new PhonemeFeatures(PFP_PALATAL, PFM_APPROXIMANT, PFP_NOART, PFF_VOICED));
		PHONEMES.put(new String("ʎ"), new PhonemeFeatures(PFP_PALATAL, PFM_APPROXIMANT, PFP_NOART, PFF_LATERAL | PFF_VOICED));
		PHONEMES.put(new String("ɰ"), new PhonemeFeatures(PFP_VELAR, PFM_APPROXIMANT, PFP_NOART, PFF_VOICED));
		PHONEMES.put(new String("ʟ"), new PhonemeFeatures(PFP_VELAR, PFM_APPROXIMANT, PFP_NOART, PFF_LATERAL | PFF_VOICED));
		PHONEMES.put(new String("ʀ"), new PhonemeFeatures(PFP_UVULAR, PFM_APPROXIMANT, PFP_NOART, PFF_VOICED));
		PHONEMES.put(new String("ᴙ"), new PhonemeFeatures(PFP_EPIGLOTTAL, PFM_APPROXIMANT, PFP_NOART, PFF_VOICED));

		// consonantal coarticulations
		PHONEMES.put(new String("w"), new PhonemeFeatures(PFP_VELAR, PFM_APPROXIMANT, PFP_BILABIAL, PFF_VOICED));
		PHONEMES.put(new String("ʍ"), new PhonemeFeatures(PFP_VELAR, PFM_APPROXIMANT, PFP_BILABIAL, PFF_NOFLAGS));
		PHONEMES.put(new String("ɥ"), new PhonemeFeatures(PFP_PALATAL, PFM_APPROXIMANT, PFP_BILABIAL, PFF_VOICED));
		PHONEMES.put(new String("ɫ"), new PhonemeFeatures(PFP_ALVEOLAR, PFM_APPROXIMANT, PFP_VELAR, PFF_LATERAL | PFF_VOICED));

		// closed (high) vowels	
		PHONEMES.put(new String("i"), new PhonemeFeatures(PFP_PALATAL, PFM_VOWEL_C, PFP_NOART, PFF_VOICED));
		PHONEMES.put(new String("y"), new PhonemeFeatures(PFP_PALATAL, PFM_VOWEL_C, PFP_BILABIAL, PFF_VOICED));
		PHONEMES.put(new String("ɨ"), new PhonemeFeatures(PFP_VELAR, PFM_VOWEL_C, PFP_NOART, PFF_VOICED));
		PHONEMES.put(new String("ʉ"), new PhonemeFeatures(PFP_VELAR, PFM_VOWEL_C, PFP_BILABIAL, PFF_VOICED));
		PHONEMES.put(new String("ɯ"), new PhonemeFeatures(PFP_UVULAR, PFM_VOWEL_C, PFP_NOART, PFF_VOICED));
		PHONEMES.put(new String("u"), new PhonemeFeatures(PFP_UVULAR, PFM_VOWEL_C, PFP_BILABIAL, PFF_VOICED));

		// near-close vowels
		PHONEMES.put(new String("ɪ"), new PhonemeFeatures(PFP_PALATAL, PFM_VOWEL_NC, PFP_NOART, PFF_VOICED));
		PHONEMES.put(new String("ʏ"), new PhonemeFeatures(PFP_PALATAL, PFM_VOWEL_NC, PFP_BILABIAL, PFF_VOICED));
		PHONEMES.put(new String("ɪ̈"), new PhonemeFeatures(PFP_VELAR, PFM_VOWEL_NC, PFP_NOART, PFF_VOICED));
		PHONEMES.put(new String("ʊ̈"), new PhonemeFeatures(PFP_VELAR, PFM_VOWEL_NC, PFP_BILABIAL, PFF_VOICED));
		PHONEMES.put(new String("ʊ"), new PhonemeFeatures(PFP_UVULAR, PFM_VOWEL_NC, PFP_BILABIAL, PFF_VOICED));

		// close-mid vowels
		PHONEMES.put(new String("e"), new PhonemeFeatures(PFP_PALATAL, PFM_VOWEL_CM, PFP_NOART, PFF_VOICED));
		PHONEMES.put(new String("ø"), new PhonemeFeatures(PFP_PALATAL, PFM_VOWEL_CM, PFP_BILABIAL, PFF_VOICED));
		PHONEMES.put(new String("ɘ"), new PhonemeFeatures(PFP_VELAR, PFM_VOWEL_CM, PFP_NOART, PFF_VOICED));
		PHONEMES.put(new String("ɵ"), new PhonemeFeatures(PFP_VELAR, PFM_VOWEL_CM, PFP_BILABIAL, PFF_VOICED));
		PHONEMES.put(new String("ɤ"), new PhonemeFeatures(PFP_UVULAR, PFM_VOWEL_CM, PFP_NOART, PFF_VOICED));
		PHONEMES.put(new String("o"), new PhonemeFeatures(PFP_UVULAR, PFM_VOWEL_CM, PFP_BILABIAL, PFF_VOICED));

		// mid vowels
		PHONEMES.put(new String("ə"), new PhonemeFeatures(PFP_VELAR, PFM_VOWEL_M, PFP_NOART, PFF_VOICED));
		PHONEMES.put(new String("ɚ"), new PhonemeFeatures(PFP_VELAR, PFM_VOWEL_M, PFP_NOART, PFF_VOICED));

		// open-mid vowels
		PHONEMES.put(new String("ɛ"), new PhonemeFeatures(PFP_PALATAL, PFM_VOWEL_OM, PFP_NOART, PFF_VOICED));
		PHONEMES.put(new String("œ"), new PhonemeFeatures(PFP_PALATAL, PFM_VOWEL_OM, PFP_BILABIAL, PFF_VOICED));
		PHONEMES.put(new String("ɜ"), new PhonemeFeatures(PFP_VELAR, PFM_VOWEL_OM, PFP_NOART, PFF_VOICED));
		PHONEMES.put(new String("ɝ"), new PhonemeFeatures(PFP_VELAR, PFM_VOWEL_OM, PFP_NOART, PFF_VOICED));
		PHONEMES.put(new String("ɞ"), new PhonemeFeatures(PFP_VELAR, PFM_VOWEL_OM, PFP_BILABIAL, PFF_VOICED));
		PHONEMES.put(new String("ʌ"), new PhonemeFeatures(PFP_UVULAR, PFM_VOWEL_OM, PFP_NOART, PFF_VOICED));
		PHONEMES.put(new String("ɔ"), new PhonemeFeatures(PFP_UVULAR, PFM_VOWEL_OM, PFP_BILABIAL, PFF_VOICED));

		// near-open vowels
		PHONEMES.put(new String("æ"), new PhonemeFeatures(PFP_PALATAL, PFM_VOWEL_NO, PFP_NOART, PFF_VOICED));
		PHONEMES.put(new String("ɐ"), new PhonemeFeatures(PFP_VELAR, PFM_VOWEL_NO, PFP_NOART, PFF_VOICED));

		// open vowels
		PHONEMES.put(new String("a"), new PhonemeFeatures(PFP_PALATAL, PFM_VOWEL_O, PFP_NOART, PFF_VOICED));
		PHONEMES.put(new String("ɶ"), new PhonemeFeatures(PFP_PALATAL, PFM_VOWEL_O, PFP_BILABIAL, PFF_VOICED));
		PHONEMES.put(new String("ä"), new PhonemeFeatures(PFP_VELAR, PFM_VOWEL_O, PFP_NOART, PFF_VOICED));
		PHONEMES.put(new String("ɑ"), new PhonemeFeatures(PFP_UVULAR, PFM_VOWEL_O, PFP_NOART, PFF_VOICED));
		PHONEMES.put(new String("ɒ"), new PhonemeFeatures(PFP_UVULAR, PFM_VOWEL_O, PFP_BILABIAL, PFF_VOICED));

		// null phoneme
		PHONEMES.put(new String("-"), new PhonemeFeatures(PFP_NOART, PFM_NOMANNER, PFP_NOART, PFF_NOFLAGS));
	}

	// IPA MODIFIERS
	public static final HashMap<String, PhonemeFeatures> MODIFIERS = 
		new HashMap<String, PhonemeFeatures>();
	static {
		// secondary articulation
		MODIFIERS.put(new String("ʷ"), new PhonemeFeatures(PFP_NOART, PFM_NOMANNER, PFP_BILABIAL, PFF_NOFLAGS));
		MODIFIERS.put(new String("ʲ"), new PhonemeFeatures(PFP_NOART, PFM_NOMANNER, PFP_PALATAL, PFF_NOFLAGS));
		MODIFIERS.put(new String("ˠ"), new PhonemeFeatures(PFP_NOART, PFM_NOMANNER, PFP_VELAR, PFF_NOFLAGS));
		MODIFIERS.put(new String("ˤ"), new PhonemeFeatures(PFP_NOART, PFM_NOMANNER, PFP_PHARYNGEAL, PFF_NOFLAGS));
		MODIFIERS.put(new String("ˀ"), new PhonemeFeatures(PFP_NOART, PFM_NOMANNER, PFP_GLOTTAL, PFF_NOFLAGS));

		// phonation / glottalization
		MODIFIERS.put(new String("ʰ"), new PhonemeFeatures(PFP_NOART, PFM_NOMANNER, PFP_NOART, PFF_ASPIRATED));
		MODIFIERS.put(new String("ʱ"), new PhonemeFeatures(PFP_NOART, PFM_NOMANNER, PFP_NOART, PFF_ASPIRATED));
		MODIFIERS.put(new String("ʼ"), new PhonemeFeatures(PFP_NOART, PFM_NOMANNER, PFP_NOART, PFF_EJECTIVE));

		// others
		MODIFIERS.put(new String("\u032f"), new PhonemeFeatures(PFP_NOART, PFM_NOMANNER, PFP_NOART, PFF_NONSYLLABIC));
		MODIFIERS.put(new String("\u0303"), new PhonemeFeatures(PFP_NOART, PFM_NOMANNER, PFP_NOART, PFF_NASAL));
		MODIFIERS.put(new String("~"), new PhonemeFeatures(PFP_NOART, PFM_NOMANNER, PFP_NOART, PFF_NASAL));
		MODIFIERS.put(new String("\u02d0"), new PhonemeFeatures(PFP_NOART, PFM_NOMANNER, PFP_NOART, PFF_LONG));
	}
}
