package simphony;

import java.io.*;
import java.util.ArrayList;
import simphony.datastruct.Pair;
import simphony.datastruct.Phoneme;
import simphony.datastruct.PhonemeString;
import simphony.algorithms.WeightedLevenshtein;
import simphony.transcriber.*;

public class Main {

	public static void main(String[] args) {
		try {
			ArgumentParser.parse(args);

			if (ArgumentParser.getMode().equals("train")) {
				if (ArgumentParser.getTranscriberFile().equals("")) {
					throw new ArgumentException("train mode: -t parameter (transcriber path) required!");
				}

				ArrayList<Pair<String, PhonemeString>> trainingData = readTrainingData(ArgumentParser.getInputFile());
				HmmTranscriber transcriber = new HmmTranscriber();
				transcriber.train(trainingData);
				saveTranscriber(transcriber, ArgumentParser.getTranscriberFile());
			}

			else if (ArgumentParser.getMode().equals("transcribe")) {
				ArrayList<String> inputData = readInput(ArgumentParser.getInputFile());
				Transcriber transcriber;
				if (ArgumentParser.getTranscriberFile().equals("")) {
					transcriber = new NaiveTranscriber();
				} else {
					transcriber = loadTranscriber(ArgumentParser.getTranscriberFile());
				}

				ArrayList<PhonemeString> outputData = new ArrayList<PhonemeString>();
				for (String word : inputData) 
					outputData.add(transcriber.transcribe(word));
				writeOutput(outputData, ArgumentParser.getOutputFile());
			}

			else if (ArgumentParser.getMode().equals("compare")) {
				if ((ArgumentParser.getCompareFile1().equals("")) || (ArgumentParser.getCompareFile2().equals(""))) {
					throw new ArgumentException("compare mode: two files to compare required!");
				}

				ArrayList<String> dataFirst = readInput(ArgumentParser.getCompareFile1());
				ArrayList<String> dataSecond = readInput(ArgumentParser.getCompareFile2());

				if (dataFirst.size() != dataSecond.size()) {
					throw new ArgumentException("compare mode: wordlists to compare have different lenghts!");
				}

				double totalDist = 0.0d;
				int numWords = dataFirst.size();
				for (int i = 0; i < numWords; i++) {
					PhonemeString phonFirst = new PhonemeString(dataFirst.get(i));
					PhonemeString phonSecond = new PhonemeString(dataSecond.get(i));
					double dist = WeightedLevenshtein.dist(phonFirst, phonSecond);
					totalDist += dist;
				}
				System.out.println(totalDist / numWords);
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	///////////////////////////////////////////////////////////////////
	// I/O functions
	///////////////////////////////////////////////////////////////////

	public static Transcriber loadTranscriber(String filename) throws IOException {
		Transcriber transcriber = null;
		ObjectInputStream in = null;
		try {
			in = new ObjectInputStream(new FileInputStream(filename));
			transcriber = (Transcriber)in.readObject();
		} catch (IOException ex) {
			throw ex;
		} finally {
			if (in != null) in.close();
			return transcriber;
		}
	}

	public static void saveTranscriber(Transcriber transcriber, String filename) throws IOException {
		ObjectOutputStream out = null;
		try {
			out = new ObjectOutputStream(new FileOutputStream(filename));
			out.writeObject(transcriber);
			out.flush();
		} catch (IOException ex) {
			throw ex;
		} finally {
			if (out != null) out.close();
		}
	}

	public static ArrayList<Pair<String, PhonemeString>> readTrainingData(String filename) throws IOException {
		ArrayList<Pair<String, PhonemeString>> trainingData = new ArrayList<Pair<String, PhonemeString>>();
		BufferedReader in = null;
		try {
			if (filename.equals(""))
				in = new BufferedReader(new InputStreamReader(System.in));
			else
				in = new BufferedReader(new FileReader(filename));

			String line;
			while ((line = in.readLine()) != null) {
				String[] split = line.split("\t");
				if (split.length == 2) 
					trainingData.add(new Pair<String, PhonemeString>(
						split[0], new PhonemeString(split[1].trim())));
			}
		} catch (IOException ex) {
			throw ex;
		} finally {
			if (in != null) in.close();
			return trainingData;
		}
	}

	public static ArrayList<String> readInput(String filename) throws IOException {
		ArrayList<String> inputData = new ArrayList<String>();
		BufferedReader in = null;
		try {
			if (filename.equals(""))
				in = new BufferedReader(new InputStreamReader(System.in));
			else
				in = new BufferedReader(new FileReader(filename));

			String line;
			while ((line = in.readLine()) != null)
				inputData.add(line.trim());
		} catch (IOException ex) {
			throw ex;
		} finally {
			if (in != null) in.close();
			return inputData;
		}
	}

	public static void writeOutput(ArrayList<PhonemeString> data, String filename) throws IOException {
		BufferedWriter out = null;
		try {
			if (filename.equals(""))
				out = new BufferedWriter(new OutputStreamWriter(System.out));
			else
				out = new BufferedWriter(new FileWriter(filename));
			for (PhonemeString phon : data) {
				out.write(phon.toString());
				out.newLine();
			}
		} catch (IOException ex) {
			throw ex;
		} finally {
			if (out != null) out.close();
		}
	}
}
