package simphony;

public class ArgumentParser {
	private static final String[] MODES = new String[] { "train", "transcribe", "compare" };
	private static String mode = "";	
	private static String inputFile = "";
	private static String outputFile = "";
	private static String transcriberFile = "";
	private static String compareFile1 = "";
	private static String compareFile2 = "";

	private static boolean silent = false;
	private static boolean verbose = false;

	private static final String OPTION_INPUT_FILE		= "-i";
	private static final String OPTION_OUTPUT_FILE		= "-o";
	private static final String OPTION_TRANSCRIBER_FILE	= "-t";
	private static final String OPTION_SILENT			= "-s";
	private static final String OPTION_VERBOSE			= "-v";

	private ArgumentParser() 	{}

	public static void parse(String[] args) throws ArgumentException {
		// get mode
		if (args.length < 1) {
			printHelpAndExit();
		}
		else if (args[0].equals("help") || args[0].equals("--help")) {
			printHelpAndExit();
		}
		else {
			for (int i = 0; i < MODES.length; i++)
				if (args[0].equals(MODES[i])) {
					mode = MODES[i];
					break;
				}
			if (mode.isEmpty()) {
				System.out.println("Invalid mode: " + args[0]);
				System.out.println("Use --help option for help.");
				System.exit(1);
			}
		}

		// get arguments
		for (int i = 1; i < args.length; i++) {
			if (args[i].equals(OPTION_INPUT_FILE)) {
				inputFile = args[i+1];
				i++;
			} else if (args[i].equals(OPTION_OUTPUT_FILE)) {
				outputFile = args[i+1];
				i++;
			} else if (args[i].equals(OPTION_TRANSCRIBER_FILE)) {
				transcriberFile = args[i+1];
				i++;
			} else if (args[i].equals(OPTION_SILENT)) {
				silent = true;
			} else if (args[i].equals(OPTION_VERBOSE)) {
				verbose = true;
			} else {
				if (mode.equals("compare") && (i+1 < args.length)) {
					compareFile1 = args[i];
					compareFile2 = args[i+1];
					break;
				} else {
					throw new ArgumentException("Unknown option: "+args[i]);
				}
			}
		}
	}

	public static String getMode() { return mode; }
	public static String getInputFile() { return inputFile; }
	public static String getOutputFile() { return outputFile; }
	public static String getTranscriberFile() { return transcriberFile; }
	public static String getCompareFile1() { return compareFile1; }
	public static String getCompareFile2() { return compareFile2; }

	public static boolean isSilent() { return silent; }
	public static boolean isVerbose() { return verbose; }

	private static void printHelpAndExit() {
		System.out.println("SIMPHONY -- PHONetic transcription and SIMilarity tool v1.0");
		System.out.println("(c) Maciej Janicki 2012-2013, University of Leipzig");
		System.out.println("");
		System.out.println("USAGE:");
		System.out.println("java -jar simphony.jar help");
		System.out.println("java -jar simphony.jar train (OPTIONS) (-i TRAINING_DATA_FILE) -t TRANSCRIBER_FILE");
		System.out.println("java -jar simphony.jar transcribe (OPTIONS) (-i INPUT_FILE) (-o OUTPUT_FILE) (-t TRANSCRIBER_FILE)");
		System.out.println("java -jar simphony.jar compare (OPTIONS) FILE_1 FILE_2");
		System.out.println("");
		System.out.println("OPTIONS:");
		System.out.println("-v\tverbose -- print additional information on output");
		System.out.println("-s\tsilent -- print nothing but results on output");
		System.exit(0);
	}
}
