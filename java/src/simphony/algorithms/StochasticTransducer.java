package simphony.algorithms;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import simphony.datastruct.Pair;
import simphony.datastruct.PhonemeString;

public class StochasticTransducer implements java.io.Serializable {
	private static final int MAX_X = 3;
	private static final int MAX_Y = 2;
	private static final boolean DEL_X = true;
	private static final boolean DEL_Y = false;
	private static final double MAXERROR_THRESHOLD = 1.0E-6;

	HashMap<Pair<String, PhonemeString>, Double> mappingCounts;
	HashMap<Pair<String, PhonemeString>, Double> mappingProbabilities;

	public StochasticTransducer() {
		this.mappingCounts = new HashMap<Pair<String, PhonemeString>, Double>();
		this.mappingProbabilities = new HashMap<Pair<String, PhonemeString>, Double>();
	}

	public void train(ArrayList<Pair<String, PhonemeString>> trainingData) {
		initProbabilities(trainingData);
		double maxError = 1.0d;
		for (int i = 0; maxError >= MAXERROR_THRESHOLD; i++) {
			System.out.println("Iteration " + i);
			expectation(trainingData);
			maxError = maximization();
			System.out.println("maxError = " + maxError);
		}
	}

	public ArrayList<Pair<String, PhonemeString>> align(String orth, PhonemeString phon) {
		ArrayList<Pair<String, PhonemeString>> alignment = new ArrayList<Pair<String, PhonemeString>>();
		int maxT = orth.length();
		int maxV = phon.length();
		double[][] alpha = new double[maxT+1][maxV+1];
		Pair<String, PhonemeString>[][] optimalMappings = new Pair[maxT+1][maxV+1];
		alpha[0][0] = 1.0;
		for (int t = 0; t <= maxT; t++)
			for (int v = 0; v <= maxV; v++) {
				HashMap<Pair<String, PhonemeString>, Double> possibleMappings = 
					new HashMap<Pair<String, PhonemeString>, Double>();
				if ((t > 0) && (DEL_X))
					for (int i = 1; i <= Math.min(MAX_X, t); i++) {
						Pair<String, PhonemeString> key = 
							new Pair<String, PhonemeString>(
								orth.substring(t-i, t), new PhonemeString(""));
						possibleMappings.put(key, this.mappingProbabilities.get(key) * alpha[t-i][v]);
					}
				if ((v > 0) && (DEL_Y))
					for (int j = 1; j <= Math.min(MAX_Y, v); j++) {
						Pair<String, PhonemeString> key = 
							new Pair<String, PhonemeString>(
								"", phon.substring(v-j, v));
						possibleMappings.put(key, this.mappingProbabilities.get(key) * alpha[t][v-j]);
					}
				if ((t > 0) && (v > 0))
					for (int i = 1; i <= Math.min(MAX_X, t); i++)
						for (int j = 1; j <= Math.min(MAX_Y, v); j++) {
							if ((i > 1) && (j > 1)) continue;
							Pair<String, PhonemeString> key = 
								new Pair<String, PhonemeString>(
									orth.substring(t-i, t), phon.substring(v-j, v));
							possibleMappings.put(key, this.mappingProbabilities.get(key) * alpha[t-i][v-j]);
						}
				if (possibleMappings.isEmpty()) continue;
				// find the mapping with highest probability
				double optimalMappingPr = 0.0;
				Pair<String, PhonemeString> optimalMapping = null;
				for (Pair<String, PhonemeString> mapping : possibleMappings.keySet()) {
					Double pr = possibleMappings.get(mapping);
					if (pr >= optimalMappingPr) {
						optimalMapping = mapping;
						optimalMappingPr = pr;
					}
				}
				optimalMappings[t][v] = optimalMapping;
				alpha[t][v] = optimalMappingPr;
			}
		int t = maxT;
		int v = maxV;
		while ((t > 0) || (v > 0)) {
			Pair<String, PhonemeString> mapping = optimalMappings[t][v];
			alignment.add(mapping);
			System.out.println("" +mapping.first + " "+mapping.second + " " + this.mappingProbabilities.get(mapping));
			t -= mapping.first.length();
			v -= mapping.second.length();
		}
		Collections.reverse(alignment);
		return alignment;
	}

	private void initProbabilities(ArrayList<Pair<String, PhonemeString>> trainingData) {
		HashSet<Pair<String, PhonemeString>> mappings = new HashSet<Pair<String, PhonemeString>>();
		for (Pair<String, PhonemeString> trainingPair: trainingData) {
			String orth = trainingPair.first;
			PhonemeString phon = trainingPair.second;
			for (int t = 0; t <= orth.length(); t++)
				for (int v = 0; v <= phon.length(); v++) {
					if ((t > 0) && (DEL_X))
						for (int i = 1; i <= Math.min(MAX_X, t); i++)
							mappings.add(
								new Pair<String, PhonemeString>(
									orth.substring(t-i, t), new PhonemeString("")));
					if ((v > 0) && (DEL_Y))
						for (int j = 1; j <= Math.min(MAX_Y, v); j++)
							mappings.add(
								new Pair<String, PhonemeString>(
									"", phon.substring(v-j, v)));
					if ((t > 0) && (v > 0))
						for (int i = 1; i <= Math.min(MAX_X, t); i++)
							for (int j = 1; j <= Math.min(MAX_Y, v); j++) {
								if ((i > 1) && (j > 1)) continue;
								mappings.add(
									new Pair<String, PhonemeString>(
										orth.substring(t-i, t), phon.substring(v-j, v)));
							}
				}
		}
		for (Pair<String, PhonemeString> mapping : mappings) {
			this.mappingCounts.put(mapping, new Double(1.0d / (0.1 + Math.abs(mapping.first.length()-1.0) + Math.abs(mapping.second.length()-1.0))));
			//this.mappingCounts.put(mapping, new Double(1.0d));
		}
		maximization();
	}

	private void expectation(ArrayList<Pair<String, PhonemeString>> trainingData) {
		for (Pair<String, PhonemeString> mapping : this.mappingCounts.keySet())
			this.mappingCounts.put(mapping, new Double(0.0d));
		for (Pair<String, PhonemeString> trainingPair : trainingData) {
			String orth = trainingPair.first;
			PhonemeString phon = trainingPair.second;
			double[][] alpha = forward(orth, phon);
			double[][] beta = backward(orth, phon);
			double totalPr = alpha[orth.length()][phon.length()];
			if (totalPr == 0) continue;
			for (int t = 0; t <= orth.length(); t++)
				for (int v = 0; v <= phon.length(); v++) {
					if ((t > 0) && (DEL_X))
						for (int i = 1; i <= Math.min(MAX_X, t); i++) {
							Pair<String, PhonemeString> key = 
								new Pair<String, PhonemeString>(
									orth.substring(t-i, t), new PhonemeString(""));
							Double count = this.mappingCounts.get(key);
							count += alpha[t-i][v] * this.mappingProbabilities.get(key) * beta[t][v] / totalPr;
							this.mappingCounts.put(key, count);
						}
					if ((v > 0) && (DEL_Y))
						for (int j = 1; j <= Math.min(MAX_Y, v); j++) {
							Pair<String, PhonemeString> key = 
								new Pair<String, PhonemeString>(
									"", phon.substring(v-j, v));
							Double count = this.mappingCounts.get(key);
							count += alpha[t][v-j] * this.mappingProbabilities.get(key) * beta[t][v] / totalPr;
							this.mappingCounts.put(key, count);
						}
					if ((t > 0) && (v > 0))
						for (int i = 1; i <= Math.min(MAX_X, t); i++)
							for (int j = 1; j <= Math.min(MAX_Y, v); j++) {
								if ((i > 1) && (j > 1)) continue;
								Pair<String, PhonemeString> key = 
									new Pair<String, PhonemeString>(
										orth.substring(t-i, t), phon.substring(v-j, v));
								Double count = this.mappingCounts.get(key);
								count += alpha[t-i][v-j] * this.mappingProbabilities.get(key) * beta[t][v] / totalPr;
								this.mappingCounts.put(key, count);
							}
			}
		}
	}

	// returns the mean squared error between current and previous probabilities
	private double maximization() {
		double mappingCountsSum = 0.0d, maxError = 0.0d;
		for (Double count : this.mappingCounts.values())
			mappingCountsSum += count;
		for (Pair<String, PhonemeString> key: this.mappingCounts.keySet()) {
			double oldProbability;
			if (this.mappingProbabilities.containsKey(key))
				oldProbability = this.mappingProbabilities.get(key);
			else
				oldProbability = 0.0d;
			double newProbability = this.mappingCounts.get(key) / mappingCountsSum;
			//this.mappingProbabilities.put(key, this.mappingCounts.get(key) / mappingCountsSum);
			this.mappingProbabilities.put(key, new Double(newProbability));
			double error = Math.abs(newProbability - oldProbability);
			if (error > maxError)
				maxError = error;
		}
		return maxError;
	}

	private double[][] forward(String orth, PhonemeString phon) {
		int maxT = orth.length();
		int maxV = phon.length();
		double[][] alpha = new double [maxT+1][maxV+1];
		alpha[0][0] = 1.0;
		for (int t = 0; t <= maxT; t++)
			for (int v = 0; v <= maxV; v++) {
				if ((t > 0) || (v > 0))
					alpha[t][v] = 0.0;
				if ((t > 0) && (DEL_X))
					for (int i = 1; i <= Math.min(MAX_X, t); i++) {
						Pair<String, PhonemeString> key = 
							new Pair<String, PhonemeString>(
								orth.substring(t-i, t), new PhonemeString(""));
						alpha[t][v] += this.mappingProbabilities.get(key) * alpha[t-i][v];
					}
				if ((v > 0) && (DEL_Y))
					for (int j = 1; j <= Math.min(MAX_Y, v); j++) {
						Pair<String, PhonemeString> key = 
							new Pair<String, PhonemeString>(
								"", phon.substring(v-j, v));
						alpha[t][v] += this.mappingProbabilities.get(key) * alpha[t][v-j];
					}
				if ((t > 0) && (v > 0))
					for (int i = 1; i <= Math.min(MAX_X, t); i++)
						for (int j = 1; j <= Math.min(MAX_Y, v); j++) {
							if ((i > 1) && (j > 1)) continue;
							Pair<String, PhonemeString> key = 
								new Pair<String, PhonemeString>(
									orth.substring(t-i, t), phon.substring(v-j, v));
							alpha[t][v] += this.mappingProbabilities.get(key) * alpha[t-i][v-j];
						}
			}
		return alpha;
	}

	private double[][] backward(String orth, PhonemeString phon) {
		int maxT = orth.length();
		int maxV = phon.length();
		double[][] beta = new double [maxT+1][maxV+1];
		beta[maxT][maxV] = 1.0;
		for (int t = maxT; t >= 0; t--)
			for (int v = maxV; v >= 0; v--) {
				if ((t < maxT) || (v < maxV))
					beta[t][v] = 0.0;
				if ((t < maxT) && (DEL_X))
					for (int i = 1; i <= Math.min(MAX_X, maxT-t); i++) {
						Pair<String, PhonemeString> key = 
							new Pair<String, PhonemeString>(
								orth.substring(t, t+i), new PhonemeString(""));
						beta[t][v] += this.mappingProbabilities.get(key) * beta[t+i][v];
					}
				if ((v < maxV) && (DEL_Y))
					for (int j = 1; j <= Math.min(MAX_Y, maxV-v); j++) {
						Pair<String, PhonemeString> key = 
							new Pair<String, PhonemeString>(
								"", phon.substring(v, v+j));
						beta[t][v] += this.mappingProbabilities.get(key) * beta[t][v+j];
					}
				if ((t > 0) && (v > 0))
					for (int i = 1; i <= Math.min(MAX_X, maxT-t); i++)
						for (int j = 1; j <= Math.min(MAX_Y, maxV-v); j++) {
							if ((i > 1) && (j > 1)) continue;
							Pair<String, PhonemeString> key = 
								new Pair<String, PhonemeString>(
									orth.substring(t, t+i), phon.substring(v, v+j));
							beta[t][v] += this.mappingProbabilities.get(key) * beta[t+i][v+j];
						}
			}
		return beta;
	}
}
