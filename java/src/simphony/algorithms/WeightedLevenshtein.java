package simphony.algorithms;

import java.util.ArrayList;
import java.util.Collections;
import simphony.datastruct.Phoneme;
import simphony.datastruct.PhonemeString;

public class WeightedLevenshtein {
//	HashMap<Pair<Phoneme, Phoneme>, Double> weights = null;
//	private static final double INSERTION_COST = 1.0d;
//	private static final double DELETION_COST = 1.0d;

	private static Phoneme nullPhoneme = new Phoneme("-", "");

	private WeightedLevenshtein() {}

//	public void initWeights(HashMap<Pair<Phoneme, Phoneme>, Double> weights) {
//		this.weights = weights;
//	}

	public static double dist(PhonemeString x, PhonemeString y) {
		ArrayList<Phoneme> xp = x.getPhonemes();
		ArrayList<Phoneme> yp = y.getPhonemes();
		double[] previousRow = new double [xp.size()+1];
		String[] previousMappings = new String[xp.size()+1];
		double maxDist = 0.0d;

		// init first row
		previousRow[0] = 0.0d;
		previousMappings[0] = "";
		for (int i = 1; i < previousRow.length; i++) {
			//double deletionCost = xp.get(i-1).dist(nullPhoneme);
			double deletionCost = xp.get(i-1).abs();
			previousRow[i] = previousRow[i-1] + deletionCost;
			previousMappings[i] = previousMappings[i-1] + "D";
			maxDist += deletionCost;
		}

		// further rows
		for (int j = 1; j < yp.size()+1; j++) {
			double[] currentRow = new double [xp.size()+1];
			String[] currentMappings = new String [xp.size()+1];
			//double initialInsertionCost = yp.get(j-1).dist(nullPhoneme);
			double initialInsertionCost = yp.get(j-1).abs();
			currentRow[0] = previousRow[0] + initialInsertionCost;
			currentMappings[0] = previousMappings[0] + "I";
			maxDist += initialInsertionCost;
			for (int i = 1; i < currentRow.length; i++) {
				//double insertionCost = yp.get(j-1).dist(nullPhoneme);
				//double insertionCost = yp.get(j-1).abs();
				//double insertionCost = xp.get(i-1).dist(yp.get(j-1));
				double insertionCost;
				if (previousMappings[i].endsWith("S"))
					insertionCost = xp.get(i-1).dist(yp.get(j-1));
				else
					insertionCost = yp.get(j-1).abs();
				//double deletionCost = xp.get(i-1).dist(nullPhoneme);
				//double deletionCost = xp.get(i-1).abs();
				//double deletionCost = xp.get(i-1).dist(yp.get(j-1));
				double deletionCost;
				if (currentMappings[i-1].endsWith("S"))
					deletionCost = xp.get(i-1).dist(yp.get(j-1));
				else
					deletionCost = xp.get(i-1).abs();
				double substitutionCost = xp.get(i-1).dist(yp.get(j-1));
				currentRow[i] = min3(
					currentRow[i-1] + deletionCost,
					previousRow[i] + insertionCost,
					previousRow[i-1] + substitutionCost
				);
				if (currentRow[i] == currentRow[i-1] + deletionCost)
					currentMappings[i] = currentMappings[i-1]+"D";
				else if (currentRow[i] == previousRow[i] + insertionCost)
					currentMappings[i] = previousMappings[i]+"I";
				else if (currentRow[i] == previousRow[i-1] + substitutionCost)
					currentMappings[i] = previousMappings[i-1]+"S";
			}
			previousRow = currentRow;
			previousMappings = currentMappings;
		}
		
		String alignment = previousMappings[previousMappings.length-1];
        ArrayList<Double> mappingCosts = new ArrayList<Double>();
		int i = -1, j = -1;
		for (int n = 0; n < alignment.length(); n++) {
			if (alignment.charAt(n) == 'S') {
				i++; j++;
				System.out.println(""+xp.get(i)+":"+yp.get(j)+"\t"+xp.get(i).dist(yp.get(j)));
                mappingCosts.add(new Double(xp.get(i).dist(yp.get(j))));
			} else if (alignment.charAt(n) == 'I') {
				//System.out.println("-:"+yp.get(j)+"\t"+yp.get(j).abs());
				j++;
				if ((n > 0) && (alignment.charAt(n-1) == 'S')) {
					System.out.println("-:"+yp.get(j)+"\t"+xp.get(i).dist(yp.get(j)));
					mappingCosts.add(new Double(xp.get(i).dist(yp.get(j))));
                }
				else {
					System.out.println("-:"+yp.get(j)+"\t"+yp.get(j).abs());
					mappingCosts.add(new Double(yp.get(j).abs()));
                }
			} else if (alignment.charAt(n) == 'D') {
				//System.out.println(""+xp.get(i)+":-\t"+xp.get(i).abs());
				i++;
				if ((n > 0) && (alignment.charAt(n-1) == 'S')) {
					System.out.println(""+xp.get(i)+":-\t"+xp.get(i).dist(yp.get(j)));
					mappingCosts.add(new Double(xp.get(i).dist(yp.get(j))));
				}
				else {
					System.out.println(""+xp.get(i)+":-\t"+xp.get(i).abs());
					mappingCosts.add(new Double(xp.get(i).abs()));
				}
			}
		}
		Collections.sort(mappingCosts);
		
		// TODO maximal distance -- sum of all insertion and deletion probabilities
		//return previousRow[previousRow.length-1] / previousMappings[previousMappings.length-1].length();
		// TODO max(mean, median)?
		return mappingCosts.get(mappingCosts.size() / 2);
	}

	private static double min3(double a, double b, double c) {
		return Math.min(Math.min(a, b), c);
	}
}
