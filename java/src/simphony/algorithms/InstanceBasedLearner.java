package simphony.algorithms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import simphony.datastruct.NGramWithContext;
import simphony.datastruct.Pair;

public class InstanceBasedLearner<LabelType> implements java.io.Serializable {
	HashSet<Pair<NGramWithContext, LabelType>> instances;

	public InstanceBasedLearner() {
		this.instances = new HashSet<Pair<NGramWithContext, LabelType>>();
	}

	public void addInstances(ArrayList<Pair<NGramWithContext, LabelType>> instances) {
		for (Pair<NGramWithContext, LabelType> instance : instances)
			this.instances.add(instance);
	}

	public LabelType classify(NGramWithContext instance) {
		LabelType label = null;
		double minDist = 1.0;
		for (Pair<NGramWithContext, LabelType> myInstance : this.instances) {
			double dist = myInstance.first.distance(instance);
			if (dist <= minDist) {
				minDist = dist;
				label = myInstance.second;
			}
		}
		// TODO implement majority vote in case of tie?
		return label;
	}

	public HashMap<LabelType, Double> allLabels(NGramWithContext instance) {
		HashMap<LabelType, Double> labels = new HashMap<LabelType, Double>();
		for (Pair<NGramWithContext, LabelType> myInstance : this.instances) {
			if (myInstance.first.getNGram().equals(instance.getNGram())) {
				double dist = myInstance.first.distance(instance);
				if ((!labels.containsKey(myInstance.second)) || (dist <= labels.get(myInstance.second)))
					labels.put(myInstance.second, new Double(dist));
				//System.out.println(myInstance.second.toString() + " " + dist);
			}
		}
		return labels;
	}
}
