package simphony;

public class ArgumentException extends Exception {
	public ArgumentException(String msg) {
		super(msg);
	}
}
